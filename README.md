# **Manual GenomeProfile**

April 2024

**Martijn Derks**

**Ina Hulsegge**

Wageningen University & Research

Animal Breeding & Genomics

## Description

The software **GenomeProfile** described in this manual provides a genome profile of data reported in final reports of Affymetrix or Illumina SNP arrays or PLINK ped files. The tool runs copy number variation analysis (PennCNV), aneuploidy screen (PloidyScreen), inbreeding, population stratification and genetic distances analysis (PLINK) and Runs of Homozygosity analysis (PLINK + DetectRUNS).

This manual is for GenomeProfile Version 0.4, released in August 2023. A Linux version is readily available named 'GenomeProfile\_V0.4'. The pipeline is made in Python 3.9.4, depends on R 4.2.0, and can be run using the master "GenomeProfile.py" script. 

_**Running in HPC environment:**_
To start running the program, you can submit the GP.sh script to slurm on the HPC. This script should contain following steps:
```
## load modules and install dependencies
module load python/3.9.4
module load R/4.2.0
pip install -r requirements.txt

## Run the genome profile tool
python GenomeProfile.py
```

The program GenomeProfile.py will look for a parameter file named 'GenomeProfile.inp' in the directory where you are running the program (details on this file are provided below). If the executable is located in a different folder than where the job is run, the full path to that folder should be specified before the name of the executable.


_**Running in other environment:**_
Install python version 3.9.4 ([text](https://www.python.org/downloads/release/python-394/)) and R version 4.2.0 ([text](https://cran.r-project.org/)).

```
## load modules and install dependencies
pip install -r requirements.txt

## Run the genome profile tool
python GenomeProfile.py
```

## Terms of use

GenomeProfile is freely available.

Acknowledgement in scientific publications is highly appreciated and can be done by citing the tool

The reference for the program is:
Ina Hulsegge, Aniek C. Bouwman, Martijn F.L. Derks, 2024, GenomeProfile: Unveiling genomic profile for livestock breeding through comprehensive SNP array-based genotyping._

## Overview pipeline

Figure 1 shows an overview of the GenomeProfile pipeline. The tool needs a **map file** with SNP positions and a FinalReport file (e.g. From GenomeStudio) or PLINK ped file to run. An example map file is shown below:

```
Name	Chr	Pos
ALGA0000009	1	538161
ALGA0000014	1	565627
ALGA0000021	1	861439
ALGA0000022	1	865364
ALGA0000046	1	1314590
ALGA0000047	1	1300464
ALGA0000112	1	3327918
ALGA0000120	1	3414845
ALGA0000131	1	3485019
```

An example **FinalReport file** is given below, this file can also include a header which will be automatically discarded:

```
Sample ID	SNP Name	Allele1 - Top	Allele2 - Top	X	Y	B Allele Freq	Log R Ratio
137366	1_10673082	A	A	0.741	0.065	0.0078	0.0259
137367	1_10673082	A	A	0.709	0.064	0.0093	-0.0356
137368	1_10673082	A	A	0.789	0.05	0.0000	0.0946
137369	1_10673082	A	G	0.476	0.612	0.4932	0.1332
137370	1_10673082	A	G	0.447	0.637	0.5343	0.1312
137371	1_10673082	A	A	0.778	0.041	0.0000	0.0645
137372	1_10673082	A	G	0.407	0.583	0.5366	0.0008
137373	1_10673082	A	G	0.404	0.587	0.5426	0.0035
137374	1_10673082	A	G	0.416	0.627	0.5581	0.0807
137375	1_10673082	A	G	0.429	0.609	0.5327	0.0682
137377	1_10673082	A	G	0.436	0.559	0.4924	0.0048

```


An example **PLINK ped file** is given below:

```
NF 137221 0 0 0 -9 A A A A A A A A C C G G G G C C A A G G C C G
NF 137222 0 0 0 -9 A A A G A A A C C C G G G G A C A A G G C A G
NF 137223 0 0 0 -9 A A A G A A A C C C G G G G A C A A G G C A G
NF 137224 0 0 0 -9 G A G G C A C C A C A G A G A C G A G G C A A
NF 137225 0 0 0 -9 A A G G A A C C C C G G G G A A A A G G A A G
NF 137226 0 0 0 -9 A A G G A A C C C C G G G G A A A A G G A A G
NF 137227 0 0 0 -9 A A G G A A C C C C G G G G A A A A G G A A G
NF 137228 0 0 0 -9 A A G G A A C C C C G G G G A A A A G G A A G
```


![Figure1](figures/Figure1.png)

_Figure 1: Overview of the GenomeProfile pipeline._

**Optional breedcode file:**

The pipeline can also read a breedcode file in which every animal in the FinalReport file is given a breedcode, this can be useful to compare breeds or to give a specific name to the breed under study. Note that if you have multiple FinalReport files you need to concatenate the samples into a single breedcode file. An example breedcode file:

```
Name	Breed
137221	NF
137222	NF
137223	NF
137224	NF
137225	NF

```

The pipeline first converts the input files to plink format to filter out SNPs and animals that have a low call rate (given the threshold provided in the GenomeProfile.inp file). The pipeline subsequently runs PennCNV (Wang et al., 2007) and PloidyScreen (Bouwman et al., 2023) to check for copy number variation and aneuploidy using the X and Y values and the B allele frequency (BAF) and Log R-Ratio (LRR). Subsequently the tool uses Plink (Purcell et al., 2007) format (.ped and .map) to calculate an inbreeding coefficient. This coefficient is obtained using the --het function in Plink which computes observed and expected autosomal homozygous genotype counts for each sample, and reports method-of-moments F coefficient estimates. Next, the pipeline performs ROH analysis using Plink which is further visualized using the detectRUNS R package (Biscarini et al., 2019).

## The parameter file 'GenomeProfile.inp'

The default name of the parameter file is 'GenomeProfile.inp'. This file specifies all input parameters and input files. It has the following format:

| Line number | Signification |
| --- | --- |
| 1 | Directory name where FinalReport files are located |
| 2 | Pathway and name of the map file |
| 3 | Chromosome(s) to exclude from classification (separated by a comma) |
| 4 | Call Rate below which samples will be excluded from classification |
| 5 | Call Rate below which SNPs will be excluded |
| 6 | Breedcode file |
| 7 | Species (e.g. chicken, pig, cow) |
| 8 | File format (1 for FinalReport, 2 for Plink ped) |

Example GenomeProfile.inp file:

```
/your_directory/
/your_directory/SNP_Map.txt
0,X,UWGS,Y
0.9
0.9
/your_directory/Breedcode.txt
Pig
1

```

## The parameter file 'ROH.inp'

This file specifies all input parameters for ROH analysis in Plink. It has the following format:

| Line number | Signification |
| --- | --- |
| 1 | Homozyg-density: The ROH must have at least one SNP per \<input\> kb on average. |
| 2 | Homozyg-window-het: Maximum number of heterozygous SNPs allowed in scanning window. |
| 3 | Homozyg-kb: minimum size of a ROH |
| 4 | Homozyg-window-snp: Scanning window size. |

Example GenomeProfile.inp file:

```
1000
1
10
30
```

## Map file

The map file has the following format:

| Column number | Column name |
| --- | --- |
| 1 | Name |
| 2 | Chr |
| 3 | Pos |

Name=SNP name; Chr = Chromosome number; Pos = position

The column names must have exactly the same names as mentioned above. The file needs to be tab delimited. The Names should match the naming of the SNP in the FinalReport file.

## FinalReport file

FinalReport files from genotype providers can be read in if filename ends with '\_FinalReport.txt' (tab delimited) or '\_FinalReport.csv', and needs to be unzipped (for now). A header can be in the file but is not required, the number of header lines doesn't matter, as long as the last header line contains the column names starting with SNP (Name). The FinalReport file must include eight essential columns which can be exported directly from GenomeStudio. Any other columns are allowed. The eight mandatory columns are:

```
SNP Name
Sample ID (or Sample Name)
Allele1 – Top
Allele2 – Top
X
Y
B Allele Freq
Log R Ratio.
```

For the column names SNPname and SampleID, spaces and underscores in the name are allowed, For example SNP\_name or Sample ID.

## PLINK ped file
PLINK ped files from genotype providers can be read in if filename ends with '.ped'

## Output

The main output is given in a summary file called output.md. This file summarizes the output from the different tools, an example output file for two samples is given below:

![Figure2](figures/Figure2.png)

The summary file shows the call rate, the number of CNVs (#CNV), number of ROHs (#ROH) and average length (KBAVG), inbreeding value (F), and the number of aneuploidy events (#monosomy, #disomy, #trisomy). Further output is provided in output directories from the individual tools:

### Principal Component Analysis: ###
In the folder containing the FinalReport file(s) a directory is created named `PCA_output`. Within this directory the results of the Principal Component Analysis (PCA) is is listed in `dataForPCA.eigenvec` and `dataForPCA.eigenval`, and visualized in the file `pca_report.pdf`. Please consult this .log file when encountering issues, and include it in mailings when reporting issues.

![Figure8](figures/Figure8.png)

_Figure 2: Principal component analysis (PCA) for 4 different breeds._

### Genetic Distances Analysis: ###
The individual pairwise genetic distance is computed by 1-ibs distance using the autosomal whole-genome variants with the command (--distance square 1-ibs) in PLINK. A distance matrix are written to .mdist, and IDs are written to .mdist.id. The main result of the genetic distance analysis is listed in plink_IBS_report.pdf.

![Figure9](figures/Figure9.png)

_Figure 3. Neighbor-joining phylogenetic tree (genetic distances)._

### PloidyScreen: ###

In the folder containing the FinalReport file(s) a directory is created named 'resultsPloidyScreen`. Within this directory there are 3 folders: 'monosomy', 'trisomy', 'others'. The XY intensity plots of chromosomes classified as monosomy or trisomy are placed in those respective folders and named \<SampleID\>\_\<chromosomenumber\>.png. In the results folder there will also be a file named 'results.txt' containing the results for each sample and chromosome combination indicating whether it is disomy, monosomy, or trisomy. In the folder containing the FinalReport file(s) a 'GenomeProfile.log' file is created reporting the progress of the screening and error messages. Please consult this .log file when encountering issues, and include it in mailings when reporting issues.

![Figure3](figures/Figure3.png)

_Figure 4: Example XY intensity plots of two chromosomes, one showing trisomy (left) and one normal (right)._

### PennCNV ###

In the folder containing the FinalReport file(s) a directory is created named 'PennCNV\_output'. Within this directory the main result is listed in `hmm\_minsnp\_3.goodcnv`. The tab delimited file shows the results in following format:

```
Locus: Locus (chr:start-stop) where the CNV is called
#SNPs: Number of SNPs within the CNV
Length: Length of the CNV
State: State (i.e. copy number) of the CNV
Sample: Sample ID
StartSNP: Start SNP name
EndSNP: End SNP name
```

In the PennCNV\_output folder a 'hmm.minsnp\_3.log' file is created reporting the progress of the screening and error messages. Please consult this .log file when encountering issues with PennCNV, and include it in mailings when reporting issues. Direct visualization of the CNVs is not yet implemented and will be part of the next version of GenomeProfile.

Example output:

| **Locus** | **#SNPs** | **Length** | **State** | **Sample** | **StartSNP** | **EndSNP** |
| --- | --- | --- | --- | --- | --- | --- |
| chr9:69445166-69654079 | numsnp=5 | length=208,914 | state1,cn=0 | 137238 | WU_10.2_9_69445166 | WU_10.2_9_69654079 |
| chr10:861539-1253632 | numsnp=5 | length=392,094 | state5,cn=3 | 137265 | WU_10.2_10_861539 | WU_10.2_10_1253632 |

### Inbreeding ###

In the folder containing the FinalReport file(s) a directory is created named 'plink\_output'. Within this directory the main result is listed in `plink.het` and visualized in the file `plink.het.pdf`. The file shows the inbreeding value per animal and population.

![Figure4](figures/Figure4.png)

_Figure 5: Figure showing inbreeding values in four different breeds._

### Runs of Homozygosity: ###

In the folder containing the FinalReport file(s) a directory is created named 'plink\_output`. Within this directory the main result of the ROH analysis is listed in plink\_ROH\_report.pdf. The report shows the ROH results in several ways. First the ROHs are shown per animal (rows) per chromosome:

![Figure5](figures/Figure5.png)

_Figure 6: ROH results in four breeds (colors) per chromosome, coloured bars show location of ROHs per individual (rows). Dark coloured areas indicate locations of common ROHs that indicate potential selective sweeps._

Also an Froh value is provides which is the proportion of the genome covered in ROHs, this can also be used as a measure of homozygosity (or inbreeding) in the population:

![Figure6](figures/Figure6.png)

_Figure 7: Froh in four different breeds._

Further results include Froh values per chromosome and Manhattan plots of ROH locations showing potential sweeps as peaks:

![Figure7](figures/Figure7.png)

_Figure 8: Manhattan plot showing potential sweep regions in one breed._

Further output can be found in the same directory. The list of files is given below:

```
.hom file: Runs of homozygosity list.
.hom.indiv: Sample-based runs-of-homozygosity report
.hom.overlap: Run-of-homozygosity pool list
.hom.summary: SNP-based runs-of-homozygosity report
```

In the folder containing the FinalReport file(s) or PLINK ped file(s) a slurm '.txt' file is created reporting the progress of the screening and error messages when running the pipeline. **Please consult this .log file when encountering issues, and include it in mailings when reporting issues.**
