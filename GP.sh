#!/bin/bash
#SBATCH --time=2-0:0:0
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --error=error_GP.txt
#SBATCH --job-name=GenomeProfile
#SBATCH --mem=180000

## load modules and install dependencies
module load python/3.9.4
module load R/4.2.0

## install dependencies python
pip install -r requirements.txt

##install dependencies R
Rscript -e 'install.packages("ape", repos="http://cran.r-project.org")'

#Make tools executable
chmod +x tools/*.exe
chmod +x tools/plink

## Run the genome profile tool
python GenomeProfile.py
