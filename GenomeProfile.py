# !/usr/bin/env python
"""
Title: Genome profile.Pipeline to assess animals that have different genome profile
Author: Wageningen Livestock Research

Version 0.4

Date: 19-05-2022
Last update: 18-04-2024

Python version: 3.9.4

Input: Illumina "Final Report", containing snpname, sampleID, Allele1 - Top, 
       Allele2 - Top, B Allele Freq, Log R Ratio, intensity data (X, Y)
       supplementary data, containing sampleID and call rate
       map file, containing snpname,chromosome and position
Output: Aneuploidy, Copy number variants (gains / losses),
        Runs of homozygosity (selective sweeps), Inbreeding
"""

import glob
import os.path
import sys
# Importing necessary libraries and uploading files
import warnings


import numpy as np
import pandas as pd
import csv
from natsort import natsort_keygen
from tensorflow.python.util import deprecation

from tools.runPCA import runPCA
from tools.runPennCNV import runPennCNV
from tools.runPloidyScreen import runPloidyScreen
from tools.runROH import runPlink

# ingnore Futurewarnings
warnings.simplefilter(action="ignore", category=FutureWarning)


# INFO and WARNING messages are not printed
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
os.environ["PYTHONWARNINGS"] = "default"
deprecation._PRINT_DEPRECATION_WARNINGS = False



# Search function
def search_string_in_file(filename, string_to_search):
    """
            Search for the given string in file and return line number 
            containing that string,
    Inputs:
    * param filename: path + name map file
    * param string_to_search: string to search in file
    Returns:
    * line number
    *
    """
    number = 0
    # Open the file in read only mode
    with open(filename, "r", encoding="utf-8") as read_obj:
        # search string in file
        for line in read_obj:
            # set line to lowercase
            line = line.lower()
            # remove characters _ and space from line
            line = line.replace("_", "")
            line = line.replace(" ", "")
            number += 1
            if string_to_search in line:
                break

    return number

#identify delimiter of a file
def identify_delimiter(filename):
    with open(filename, 'r') as file:
        dialect = csv.Sniffer().sniff(file.read(1024))  # Analyze the first 1024 bytes
        return dialect.delimiter


# Read input (inp) file
def Readinp():
    """
            Read input (inp) file
    Inputs:
    * input file: "GenomeProfile.inp"
            * first line: folder finalreports
            * second line: filename map file with whole path
            * third line: chromesome not to analyse
            * fourth line: threshold value for Call Rate sample
            * fifth line: threshold value for Call Rate SNP
            * sixth line: filename breed codes
            * seventh line: assembly
    Returns:
            * Fpath: path to final reports
            * Mfile: filename map file
            * Chr: chromosome to exclude
            * CallRateSample: threshold value for Call Rate sample
            * CallRateSNP: threshold value for Call Rate SNP
            * BreedCode: filename Breedcode file
            * Assembly: name assembly
            * nChr: number of chromosomes
    """
    try:
        # read genome.inp file
        if os.path.isfile("GenomeProfile.inp"):
            with open("GenomeProfile.inp", "r", encoding="utf-8") as f:
                inp = f.readlines()
                Fpath = inp[0].rstrip()
                Mfile = inp[1].rstrip()
                Chr = inp[2].rstrip()
                CallRateSamp = inp[3].rstrip()
                CallRateSNP = inp[4].rstrip()
                BreedCode = inp[5].rstrip()
                Assembly = inp[6].rstrip().upper()
                FileType = inp[7].rstrip()

                AssNameChr = [
                    ["CATTLE", "PIG", "SHEEP", "CHICKEN", "HORSE","SALMON","TROUT"],
                    [29, 25, 27, 39, 32, 29, 29],
                ]
                n = AssNameChr[0].index(Assembly)
                nChr = AssNameChr[1][n]
                return (
                    Fpath,
                    Mfile,
                    Chr,
                    CallRateSamp,
                    CallRateSNP,
                    BreedCode,
                    Assembly,
                    nChr,
					FileType,
                )
        else:
            print("The file 'GenomeProfile.inp' does not exists")
            sys.exit()
    except Exception as inst:
        print(f"Error in readInp: {inst}")
        return None


def readFileType(data):
    try:

        if int(data[8]) == 1:
            readFinal(data[0], data[3], data[4], data[1], data[2], data[5], data[7])
        elif int(data[8]) == 2:
            readPed(data[0], data[3], data[4], data[1], data[2], data[5], data[7])
        else:
            print(data[8])
            print("Filetype could only be 1 (Finalreport) or 2 (ped/map)")
            sys.exit()		

    except Exception as inst:
        print(f"Error in readFiles: {inst}")
        return None


def readPed(Fpath, CallRateSample, CallRateSNP, Mfile, chrom, BreedCode, nChr):
    try:
        BCfile = BreedCode
        # read file with breed codes
        BreedCode = pd.read_csv(BreedCode, sep="\t")
        BreedCode["Name"] = BreedCode["Name"].apply(lambda x: str(x))
        #replace spaces in Name by underscore
        BreedCode["Name"] = BreedCode["Name"].astype(str).str.replace(" ", "")
	
		

        # Extract filenames ped files
        files = [f for f in glob.glob(Fpath + "/*.ped")]

        for filename in files:
            print("file to analyse: " + filename)
            mapname=filename.replace("ped", "map")
            if not os.path.exists(mapname):
                print("Map file ("+mapname+") does not exists!")
                sys.exit()

            #get file delimiter of file filename
            delim = identify_delimiter(mapname)	
            #read map file
            map = pd.read_csv(mapname, sep=delim, header=None)	

            #get file delimiter of file filename
            delim = identify_delimiter(filename)
            #read ped file
            ped = pd.read_csv(filename, sep=delim, header=None, dtype=str)
            ped.iloc[:, 1] = ped.iloc[:, 1].apply(lambda x: str(x))
            column_name = ped.columns[1]
		    
			# check if there are samples in ped file which are not in the
            # breedcode file
            # if not: write these samples to a file and add the samples to the 
            # variable BreedCode
            # set the breed code to "NA"
            BCnot = ped[ped.columns[1]].unique()[
                ~np.isin(ped[ped.columns[1]].unique(), BreedCode["Name"])
            ]
            BCnotStr = ", ".join([str(elem) for i, elem in enumerate(BCnot)])
            if len(BCnotStr) > 0:
                print("Samples from ped file not in breed code file: " + BCnotStr)
            else:
                print("Samples from ped file not in breed code file: 0")
            BCnot = pd.DataFrame(BCnot, columns=["Name"])
            print(BCnot)
            BCnot.insert(loc=1, column="Breed", value="None")
            np.savetxt(
                "NotInBreedCode.txt",
                BCnot,
                fmt="%s",
                header="Name	Breed",
                comments="",
                newline="\n",
            )
            frames = [BreedCode, BCnot]
            BreedCode = pd.concat(frames)


            # check for duplicated samples
            dup = ped[ped.duplicated(subset=column_name, keep="last")]
            if not dup.empty:
                print('\nDuplicate sample name(s) were found in the ped file')
                print(f'Duplicated samples are: {dup.iloc[:, 1].unique()}')
                print('Only the last one of the duplicated samples is analysed \n')
            del dup
            # delete duplicated samples and keep only the last one	
            ped.drop_duplicates(subset=column_name,keep="last",inplace=True) 
            merged_ped = pd.merge(ped, BreedCode, left_on=column_name, right_on="Name",how='left', sort=False)
            merged_ped.iloc[:,0]=merged_ped["Breed"]
            # Drop the BreedCode columns 'Name' and 'Breed' 
            merged_ped.drop(columns=['Name','Breed'], inplace=True)
			# Replace NaN values in the 'Breed' column with 0
            merged_ped.iloc[:, 0].fillna("None", inplace=True)
            if (merged_ped.shape[1]-6)/2 != map.shape[0]:
                print("The number of SNPs in the ped file is not equal to the number SNPS in the map file")
                print("Please check the ped and map file")
                sys.exit()
            else:
				# Save ped file
                pedfile=filename.replace(".ped", "_1.ped")
                mapfile=mapname.replace(".map", "_1.map")
                np.savetxt(pedfile, merged_ped, fmt="%s", newline="\n")
                np.savetxt(mapfile, map, fmt="%s", newline="\n")

            # delete samples and snps based on call rate
            file2 = os.path.splitext(pedfile)[0]
            out = file2 + "_new"
            os.system(
                r"./tools/plink \
				--file {} \
				--mind {} \
				--geno {} \
				--recode \
				--missing \
				--chr-set {} \
				--out {}".format(
                    file2,
                    round(1 - float(CallRateSample), 1),
                    round(1 - float(CallRateSNP), 1),
                    nChr,
                    out,
                )
            )

            # delete some output files from plink not needed for further analysis
            #os.remove(os.path.splitext(file2)[0] + "_new.nosex")

            # run the analysis
            # run PCA
            runPCA(Fpath, out, nChr)
            # run plink for ROH and inbreeding
            file3 = os.path.split(file2)[1]
            runPlink(Fpath, file3, nChr)

           # create summary statistics table
            print("Start: creating summary statistics table")
            # get columns breed, animal_id and call_rate from plink results
            stats = pd.read_csv(
                file2 + "_new.imiss",
                sep=r"\s+",
                usecols=["Breed", "Animal_ID", "Call_Rate"],
                names=[
                    "Breed",
                    "Animal_ID",
                    "MISS_PHENO",
                    "N_MISS",
                    "N_GENO",
                    "Call_Rate",
                ],
                low_memory=True,
                skiprows=1,
            )
            stats["Call_Rate"] = 1 - stats["Call_Rate"]
            stats = stats.astype({"Animal_ID": "string"})
           # add #ROH and KBAVG from ROH (plink) to table
            file = (
                path
                + "plink_output_"
                + os.path.splitext(os.path.basename(file2))[0]
                + "/"
                + os.path.splitext(os.path.basename(file2))[0]
                + "_plink.hom.indiv"
            )
            print(file)
            if os.path.isfile(file) is True:
                tmp = pd.read_csv(
                    file,
                    sep=r"\s+",
                    usecols=["Animal_ID", "#ROH", "KBAVG"],
                    names=["Breed", "Animal_ID", "PHE", "#ROH", "KB", "KBAVG"],
                    skiprows=1,
                )
                tmp = tmp.astype({"Animal_ID": "string"})
                stats = pd.merge(stats, tmp, on="Animal_ID", how="left")

            else:
                stats["#ROH"] = ""
                stats["KBAVG"] = ""

            # add F from Het (plink) to table
            file = (
                path
                + "plink_output_"
                + os.path.splitext(os.path.basename(file2))[0]
                + "/"
                + os.path.splitext(os.path.basename(file2))[0]
                + "_plink.het"
            )
            if os.path.isfile(file) is True:
                tmp = pd.read_csv(
                    file,
                    sep=r"\s+",
                    usecols=["Animal_ID", "F"],
                    names=["Breed", "Animal_ID", "O(HOM)", "E(HOM)", "N(NM)", "F"],
                    skiprows=1,
                )
                tmp = tmp.astype({"Animal_ID": "string"})
                stats = pd.merge(stats, tmp, on="Animal_ID", how="left")
            else:
                stats["F"] = ""

            stats = stats.fillna("")

            html = stats.to_markdown(tablefmt="grid", index=False)

            with open(
                "output_" + os.path.splitext(os.path.basename(file2))[0] + ".md",
                "w",
                encoding="utf-8",
            ) as outfile:
                outfile.write("\n")
                outfile.write(html)
                outfile.close()
            print("Done: summary statistics table created \n\n")
 

    except Exception as inst:
        print(f"Error in readPed: {inst}")
        return None

		


def readFinal(Fpath, CallRateSample, CallRateSNP, Mfile, chrom, BreedCode, nChr):
    try:
        # read map file
        print("Reading map file: " + Mfile)
        pdmap = pd.read_csv(Mfile, sep="\t")
        # check if column names in mapfile exists
        if len(pdmap.columns) < 3:
            print("Error in mapfile " + Mfile)
            print("Number of columns in map file is not correct")
            sys.exit()
        colnames = ["Name", "Chr", "Pos"]

        for i in colnames:
            if {i}.isdisjoint(pdmap.columns):
                print("Error in read map file " + Mfile)
                print(
                    'The column "'
                    + i
                    + '" is missing in the map file or column name "'
                    + i
                    + '" is not correct'
                )
                sys.exit()
        pdmap.columns = ["snpname", "chromosome", "position"]
        pdmap = pdmap[~pdmap["chromosome"].isin(list(chrom.split(",")))]
        pdmap = pdmap[~pdmap["chromosome"].astype(str).str.startswith("Un_NW")]
        pdmap.dropna(inplace=True)
        BCfile = BreedCode
        # read file with breed codes
        BreedCode = pd.read_csv(BreedCode, sep="\t")
        BreedCode["Name"] = BreedCode["Name"].apply(lambda x: str(x))
        #replace spaces in Name by underscore
        BreedCode["Name"] = BreedCode["Name"].astype(str).str.replace(" ", "")

        # delete file output.md if exists
        if os.path.isfile("output.md") is True:
            os.remove("output.md")

        # Extract filenames finalreports
        files = [f for f in glob.glob(Fpath + "/*_FinalReport*.*")]
        print("Number of final report found in " + Fpath + ": " + str(len(files)))
        for filename in files:
            print("file to analyse: " + filename)
            # get number of line with columns names in finalreport
            lineN1 = search_string_in_file(filename, "snpname")
            # read final report
            ext_file = os.path.splitext(filename)[1]
            if ext_file == ".csv":
                finalreport = pd.read_csv(filename, sep=",", skiprows=lineN1 - 1)
            elif ext_file == ".txt":
                finalreport = pd.read_csv(filename, sep="\t", skiprows=lineN1 - 1)
            else:
                print("Final Report is not a csv or txt file")
                sys.exit()

            # delete space and set to lowercase columnnames of finalreport
            finalreport.columns = finalreport.columns.str.strip().str.lower()
            finalreport.columns = finalreport.columns.str.replace("_", "")
            finalreport.columns = finalreport.columns.str.replace(" ", "")

            # check for columnnames "snpname","samplename","allele1-top","allele2-top" 
            # in FinalReport for PenCNV

            if 'samplename' not in finalreport.columns and 'sampleid' in finalreport.columns:
                finalreport.rename(columns={'sampleid' : 'samplename'},inplace=True)
            
            colnames = ["snpname", "samplename", "allele1-top", "allele2-top"]
            for i in colnames:
                if {i}.isdisjoint(finalreport.columns):
                    print("Error in final report " + filename)
                    print("The column " + i + " is missing in the final report")
                    return

            #replace spaces in sampleID name by underscore
            finalreport["samplename"] = finalreport["samplename"].astype(str).str.replace(" ", "")

            # create new column "geno"
            finalreport["geno"] = (
                finalreport["allele1-top"] + " " + finalreport["allele2-top"]
            )

            # print number of samples and SNPs/sample
            print(
                "====> Number of samples in final report:"
                + str(finalreport["samplename"].nunique())
            )
            print(
                "====> Number of SNPs/sample in final report:"
                + str(finalreport["snpname"].nunique())
            )
            
            # check for duplicated samples
            dup = finalreport[finalreport.duplicated(subset=['samplename', 'snpname'],keep="last")]
            if not dup.empty:
                print('\nDuplicate sample name were found in the finalreport')
                print(f'Duplicated samples are: {dup["samplename"].unique()}')
                print('Only the last one of the duplicated is analysed \n')
            del dup
            # delete duplicated samples and keep only the last one	
            finalreport = finalreport.drop_duplicates(subset=['samplename', 'snpname'],keep="last") 

            # delete chromosomes set in genome.inp file and snp not in map
            finalreport = finalreport[finalreport["snpname"].isin(pdmap["snpname"])]
            pdmap = pdmap[pdmap["snpname"].isin(finalreport["snpname"])]

            # create ped and map file
            # filename ped and map file
            file = filename.replace("_FinalReport", "")
            pedfile = os.path.splitext(file)[0] + ".ped"
            mapfile = os.path.splitext(file)[0] + ".map"

            print("### Processing and writing SNPmap file")
            SNPmap = pdmap[["chromosome", "snpname", "position"]]
            SNPmap.insert(loc=2, column="cm", value="0")
            SNPmap = SNPmap.sort_values(
                by=["chromosome", "position"], key=natsort_keygen()
            )
            SNPmap['chromosome']=SNPmap['chromosome'].astype(str)

            np.savetxt(mapfile, SNPmap, fmt="%s", newline="\n")

            # replace in finalreport "-" with "0"
            finalreport["geno"] = finalreport["geno"].replace("- -", "0 0")
            finalreport = pd.merge(finalreport, SNPmap, on="snpname")
            finalreport.sort_values(
                by=["samplename", "chromosome", "position"],
                key=natsort_keygen(),
                inplace=True,
            )
            finalreport["samplename"] = finalreport["samplename"].apply(lambda x: str(x))
            finalreport['chromosome']=finalreport['chromosome'].astype(str)



            # check if there are samples in finalreport which are not in the
            # breedcode file
            # if not: write these samples to a file and add the samples to the 
            # variable BreedCode
            # set the breed code to "NA"
            BCnot = finalreport["samplename"].unique()[
                ~np.isin(finalreport["samplename"].unique(), BreedCode["Name"])
            ]
            BCnotStr = ", ".join([str(elem) for i, elem in enumerate(BCnot)])
            if len(BCnotStr) > 0:
                print("Samples from final report not in breed code file: " + BCnotStr)
            else:
                print("Samples from final report not in breed code file: 0")
            BCnot = pd.DataFrame(BCnot, columns=["Name"])
            BCnot.insert(loc=1, column="Breed", value="None")
            np.savetxt(
                "NotInBreedCode.txt",
                BCnot,
                fmt="%s",
                header="Name	Breed",
                comments="",
                newline="\n",
            )
            frames = [BreedCode, BCnot]
            BreedCode = pd.concat(frames)



            # Count number of SNPs
            snpNo = finalreport["snpname"].nunique()
            # Count number of samples
            animalNo = finalreport["samplename"].nunique()
            # Convert column "geno" to 2D array
            geno = finalreport["geno"].to_numpy()
            geno = geno.reshape(snpNo, animalNo, order="F")

            snps = finalreport[["snpname"]].head(snpNo)
            animals = finalreport["samplename"].unique()


            # Convert 2D array to dataframe and transpose the dataframe
            geno = pd.DataFrame(geno, columns=animals, index=snps["snpname"])
            geno = geno.T
            # Add two columns (BreedCode and sampleID to geno dataframe
            geno.insert(loc=0, column="pheno", value="0 0 0 -9")
            geno.insert(loc=0, column="animalID", value=animals)
            BreedCode2 = BreedCode[BreedCode["Name"].isin(geno["animalID"])]

            geno = pd.merge(BreedCode2, geno, left_on="Name", right_on="animalID")
            geno = geno.iloc[:, 1:]


            # Save ped file
            np.savetxt(pedfile, geno, fmt="%s", newline="\n")


            # delete samples and snps based on call rate
            file2 = os.path.splitext(file)[0]
            out = os.path.splitext(file)[0] + "_new"
            os.system(
                r"./tools/plink \
				--file {} \
				--mind {} \
				--geno {} \
				--recode \
				--missing \
				--chr-set {} \
				--out {}".format(
                    file2,
                    round(1 - float(CallRateSample), 1),
                    round(1 - float(CallRateSNP), 1),
                    nChr,
                    out,
                )
            )
            # delete some output files from plink not needed for further analysis
            #os.remove(os.path.splitext(file)[0] + "_new.nosex")

            #os.remove(os.path.splitext(file)[0] + "_new.log")

            # read new map file
            pdmap2 = pd.read_csv(file2 + "_new.map", sep="\t", header=None)
            pdmap2.columns = ["chromosome", "snpname", "cm", "position"]
            finalreport = finalreport[finalreport["snpname"].isin(pdmap2["snpname"])]
            pdmap2 = pdmap2[["snpname", "chromosome", "position"]]
            Mfile = file2 + "_map2.txt"
            np.savetxt(
                Mfile,
                pdmap2,
                fmt="%s",
                newline="\n",
                header="Name	Chr	Pos",
                comments="",
                delimiter="\t",
            )

            # run the analysis
            # run PCA
            runPCA(Fpath, out, nChr)
            # run PennCNV
            runPennCNV(file, finalreport, Fpath, Mfile, nChr, BCfile)
            # run plink for ROH and inbreeding
            file3 = os.path.split(file2)[1]
            runPlink(Fpath, file3, nChr)
            # run ploidyscreen
            runPloidyScreen(finalreport, Mfile, chrom, Fpath, file)

            # create summary statistics table
            print("Start: creating summary statistics table")
            # get columns breed, animal_id and call_rate from plink results
            stats = pd.read_csv(
                file2 + "_new.imiss",
                sep=r"\s+",
                usecols=["Breed", "Animal_ID", "Call_Rate"],
                names=[
                    "Breed",
                    "Animal_ID",
                    "MISS_PHENO",
                    "N_MISS",
                    "N_GENO",
                    "Call_Rate",
                ],
                low_memory=True,
                skiprows=1,
            )
            print(stats)
            stats["Call_Rate"] = 1 - stats["Call_Rate"]
            stats = stats.astype({"Animal_ID": "string"})

            # add #cnv from PennCNV results to table
            file = os.path.splitext(os.path.basename(file2))[0]
            if (
                os.path.isfile(
                    path
                    + "/PennCNV_output_"
                    + os.path.splitext(os.path.basename(file2))[0]
                    + "/hmm.minsnp_3.rawcnv"
                )
                is True
            ):
                file = path + "PennCNV_output_" + file + "/hmm.minsnp_3.rawcnv"
                os.system(
                    """awk '{print $5}' """ + file + "| uniq --count >  CountCNVs.txt "
                    ""
                )
                tmp = pd.read_csv(
                    "CountCNVs.txt",
                    sep=r"\s+",
                    usecols=["#CNV", "Animal_ID"],
                    names=["#CNV", "Animal_ID"],
                    low_memory=True,
                )
                tmp["Animal_ID"] = (
                    tmp["Animal_ID"]
                    .str.replace("./PennCNV_tmp/", "")
                    .str.replace(".txt", "")
                )
                tmp = tmp.astype({"Animal_ID": "string"})
                stats = pd.merge(stats, tmp, on="Animal_ID", how="left")
                os.remove("CountCNVs.txt")

            else:
                stats["Call_Rate"] = ""

            # add #ROH and KBAVG from ROH (plink) to table
            file = (
                path
                + "plink_output_"
                + os.path.splitext(os.path.basename(file2))[0]
                + "/"
                + os.path.splitext(os.path.basename(file2))[0]
                + "_plink.hom.indiv"
            )
            print(file)
            if os.path.isfile(file) is True:
                tmp = pd.read_csv(
                    file,
                    sep=r"\s+",
                    usecols=["Animal_ID", "#ROH", "KBAVG"],
                    names=["Breed", "Animal_ID", "PHE", "#ROH", "KB", "KBAVG"],
                    skiprows=1,
                )
                tmp = tmp.astype({"Animal_ID": "string"})
                stats = pd.merge(stats, tmp, on="Animal_ID", how="left")

            else:
                stats["#ROH"] = ""
                stats["KBAVG"] = ""

            # add F from Het (plink) to table
            file = (
                path
                + "plink_output_"
                + os.path.splitext(os.path.basename(file2))[0]
                + "/"
                + os.path.splitext(os.path.basename(file2))[0]
                + "_plink.het"
            )
            if os.path.isfile(file) is True:
                tmp = pd.read_csv(
                    file,
                    sep=r"\s+",
                    usecols=["Animal_ID", "F"],
                    names=["Breed", "Animal_ID", "O(HOM)", "E(HOM)", "N(NM)", "F"],
                    skiprows=1,
                )
                tmp = tmp.astype({"Animal_ID": "string"})
                stats = pd.merge(stats, tmp, on="Animal_ID", how="left")
            else:
                stats["F"] = ""

            file = (
                path
                + "resultsPloidyScreen_"
                + os.path.splitext(os.path.basename(file2))[0]
                + "/results.txt"
            )
            if os.path.isfile(file) is True:
                tmp = pd.read_csv(
                    file,
                    sep="\t",
                    header=None,
                    usecols=["Animal_ID", "Aneuploidy"],
                    names=["Animal_ID", "Aneuploidy"],
                )
                tmp["Animal_ID"] = tmp["Animal_ID"].str.replace(path + "/tmp/", "")
                tmp["Animal_ID"] = (
                    tmp["Animal_ID"]
                    .str.rsplit("_", 1, expand=True)
                    .rename(columns=lambda x: "col{}".format(x + 1))
                )
                tmp = tmp.astype({"Animal_ID": "string"})
                tmp = tmp.groupby(["Animal_ID", "Aneuploidy"], as_index=False).size()
                tmp1 = tmp[tmp["Aneuploidy"] == "monosomy"]
                tmp1.pop("Aneuploidy")
                if len(tmp) > 0:
                    stats = pd.merge(stats, tmp1, on="Animal_ID", how="left")
                    stats.rename(columns={"size": "#monosomy"}, inplace=True)
                else:
                    stats["#monosomy"] = ""

                tmp1 = tmp[tmp["Aneuploidy"] == "disomy"]
                tmp1.pop("Aneuploidy")
                if len(tmp) > 0:
                    stats = pd.merge(stats, tmp1, on="Animal_ID", how="left")
                    stats.rename(columns={"size": "#disomy"}, inplace=True)
                else:
                    stats["#disomy"] = ""

                tmp1 = tmp[tmp["Aneuploidy"] == "trisomy"]
                tmp1.pop("Aneuploidy")
                if len(tmp) > 0:
                    stats = pd.merge(stats, tmp1, on="Animal_ID", how="left")
                    stats.rename(columns={"size": "#trisomy"}, inplace=True)
                else:
                    stats["#trisomy"] = ""

            stats = stats.fillna("")

            html = stats.to_markdown(tablefmt="grid", index=False)

            with open(
                "output_" + os.path.splitext(os.path.basename(file2))[0] + ".md",
                "w",
                encoding="utf-8",
            ) as outfile:
                outfile.write("\n")
                outfile.write(html)
                outfile.close()
            print("Done: summary statistics table created \n\n")

    except Exception as inst:
        print(f"Error in readFiles: {inst}")
        return None



if __name__ == "__main__":
    print("Starting...")
    data = Readinp()
    sys.stdout = open(data[0] + "/GenomeProfile.log", "w", encoding="utf-8")
    path = data[0].replace("\\", "/")
    readFileType(data)
	#readFileType(data[0], data[3], data[4], data[1], data[2], data[5], data[7],data[8])
    sys.stdout.close()
