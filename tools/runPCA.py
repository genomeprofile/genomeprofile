"""
Title: Genome profile.Pipeline to assess animals that have different genome profile
Author: Wageningen Livestock Research

Module runPCA for GenomeProfile

Date: 20-05-2023
Last update: 21-06-2023


"""

import os
import shutil


def runPCA(path, file, nChr):
    """
    Inputs:
    * param path: path name
    * param file: file for PCA in plink
    * param nChr: number of chromosomes
    Output:
    * results PCA plink
    """
    print(path)
    try:
        # Create output dir for  PCA plink
        # delete output dir PCA if exists
        if (
            os.path.isdir(
                path
                + "/PCA_output_"
                + os.path.splitext(os.path.basename(file))[0]
                + "/"
            )
            is True
        ):
            print("remove directory PCA_output")
            shutil.rmtree(
                path
                + "/PCA_output_"
                + os.path.splitext(os.path.basename(file))[0]
                + "/"
            )
        # create output directory PCA
        os.mkdir(
            path + "/PCA_output_" + os.path.splitext(os.path.basename(file))[0] + "/"
        )

        # run plink for PCA
        out = (
            path
            + "PCA_output_"
            + os.path.splitext(os.path.basename(file))[0]
            + "/dataForPCA"
        )
        print("Start plink analysis - PCA")
        os.system(
            r"./tools/plink \
			--file {} \
			--allow-no-sex \
			--pca \
			--out {} \
			--chr-set {}".format(
                file, out, nChr
            )
        )

        print("Done; Finished plink analysis - PCA")

        # Create output report
        PCA = (
            path
            + "/PCA_output_"
            + os.path.splitext(os.path.basename(file))[0]
            + "/dataForPCA.eigenvec"
        )
        Eigenval = (
            path
            + "/PCA_output_"
            + os.path.splitext(os.path.basename(file))[0]
            + "/dataForPCA.eigenval"
        )
        out = path + "/PCA_output_" + os.path.splitext(os.path.basename(file))[0]
        
        os.system("Rscript tools/PCA.Rscript " + out + " " + PCA + " " + Eigenval)

        return None

    except Exception as inst:
        print(f"Error in readFiles: {inst}")
        return None
