"""
Title: Genome profile.Pipeline to assess animals that have different genome profile 
Author: Wageningen Livestock Research

Module runPloidyScreen for GenomeProfile

Date: 10-03-2020
Last update: 06-03-2025


"""
import os.path
import shutil
import sys

import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from keras.preprocessing import image
from matplotlib import pyplot as plt


def create_dir(path, filename):
    """'
            create results directory
    Inputs:
    * param path: path
    """

    Rpath = (
        path
        + "/resultsPloidyScreen_"
        + os.path.splitext(os.path.basename(filename))[0]
        + "/"
    )
    #Rpath = Rpath.replace("_FinalReport", "")
    try:
        # d elete results dir if exists
        if os.path.isdir(Rpath) is True:
            print("remove directory resultsPloidyScreen", flush=True)
            shutil.rmtree(Rpath)

        os.mkdir(Rpath)
    except OSError:
        print("Creation of the directory " + Rpath + " failed", flush=True)
        sys.exit()
    try:
        os.mkdir(Rpath + "/monosomy")
    except OSError:
        print("Creation of the directory " + Rpath + "monosomy/ failed", flush=True)
        sys.exit()
    try:
        os.mkdir(Rpath + "/trisomy")
    except OSError:
        print("Creation of the directory " + Rpath + "trisomy/ failed", flush=True)
        sys.exit()
    try:
        os.mkdir(Rpath + "/others")
    except OSError:
        print("Creation of the directory " + Rpath + "others/ failed", flush=True)
        sys.exit()

def generate_plots(data, chr, path, model, filename):
    """
            create temporary directory for all XYplot
            generate XY plots in temp file
    Inputs:
    * param data: results finalreport from def readFiles
    * param chr: chromosome numbers to exclude from classification
    * param path: path name
    * param model: prediction model
    """

    Tpath = path + "/tmp/"
    # create temporary directory for all XYplot
    # create temporary directory for all XYplot
    try:
        # Create the temporary directory (removes if exists)
        os.makedirs(Tpath, exist_ok=True)
        print("Temporary directory created at:", Tpath, flush=True)

    except OSError as e:
        print(f"Creation of the directory {Tpath} failed: {e}", flush=True)
        sys.exit(1)
	

    # generate plots
    try:
        print("starting generating x-y plots per sample per chromosome", flush=True)
        # Filter out the chromosomes to exclude 
        data["chromosome"] = data["chromosome"].map(str)  # Convert to string
        data_filtered = data[~data["chromosome"].isin(list(chr.split(",")))]	
        # Group by sample name and chromosome
        for (sample, chrom), subset in data_filtered.groupby(["samplename", "chromosome"]):
            fig, ax = plt.subplots()
            ax.axis([0, 7, 0, 5])
            plot_filename = os.path.join(Tpath, f"{sample}_{chrom}.png")

            if subset.shape[0] > 200:
                ax.scatter(subset["x"], subset["y"], s=3, facecolors="none", edgecolors="black")
                fig.savefig(plot_filename)
                plt.close(fig)
            else:
                print(f"PloidyScreen: {sample} chromosome {chrom} not analysed due to low number of SNPs", flush=True)
                plt.close(fig)

        # go to def predict_class
        # predict classes
        predict_class(path, model, filename)
        return None

    except Exception as inst:
        print(f"Error in generate_plots: {inst}", flush=True)

def predict_class(path, model, filename):
    """
            Predict the classes
    Inputs:
    * param path: path
    * param model: prediction model
    Returns:
    * folder with result images
    * result.txt file with results
    """
    try:
        # define data_path XY plots and extension
        extension = ".png"
        # set image width and height to 224 both (also used for training the model)
        img_width, img_height = 224, 224
        # Load the pre-trained model
        # Get all the XY plot files
        temp_dir = os.path.join(path, "tmp")
        Ffiles = sorted(
            [os.path.join(temp_dir, file) for file in os.listdir(temp_dir) if file.endswith(extension)],
            key=os.path.getctime
        )
		# Load images and normalize
        x = np.array([np.array(image.load_img(f, target_size=(img_width, img_height))) / 255 for f in Ffiles])
        # predict classes of the plots
        predictions = model.predict(x)
        classes = np.argmax(predictions, axis=-1)
        results = dict(zip(Ffiles, classes))
		
		# Create the result folder
        result_dir = os.path.join(path, f"resultsPloidyScreen_{os.path.splitext(os.path.basename(filename))[0]}")
        os.makedirs(result_dir, exist_ok=True)

        # open file for writing resuts
        # Define subdirectories for different categories
        categories = ["monosomy", "trisomy", "others"]
        category_dirs = {cat: os.path.join(result_dir, cat) for cat in categories}
        for cat_dir in category_dirs.values():
            os.makedirs(cat_dir, exist_ok=True)

        # Write results and copy files based on the predicted classes
        result_file = os.path.join(result_dir, "results.txt")
        with open(result_file, "a+", encoding="utf-8") as hs:
            for plot_file, class_idx in zip(Ffiles, classes):
                category = categories[class_idx]
                hs.write(f"{plot_file}\t{category}\n")
                shutil.copy(plot_file, category_dirs[category])

        # Clean up temporary directory
        shutil.rmtree(temp_dir)
        return None

    except Exception as inst:
        print(f"Error in predict_class: {inst}", flush=True)

def runPloidyScreen(finalreport, Mfile, chr, Fpath, filename):
    try:
        # runPloidyScreen

        # load model
        model = tf.keras.models.load_model("./model/PloidyScreen_V1.0.h5", compile=True)

        print("Reading map file: " + Mfile, flush=True)
        pdmap = pd.read_csv(Mfile, sep="\t")
        if len(pdmap.columns) != 3:
            print("Error in mapfile " + Mfile, flush=True)
            print("Number of columns in map file is not correct", flush=True)
            print("Map file should be tab delimited", flush=True)
            return None
        pdmap.columns = ["snpname", "chromosome", "position"]
        # check for columnnames "snpname", "samplename", "x", "y" in FinalReport
        colnames = ["snpname", "samplename", "x", "y"]
        for i in colnames:
            if {i}.isdisjoint(finalreport.columns):
                print("final report " + filename, flush=True)
                print("The column " + i + " is missing in the final report", flush=True)
                print("No PloidyScreen analysis done", flush=True)
                return None

        resultFinal = finalreport[
            ["snpname", "samplename", "chromosome", "position", "x", "y"]
        ]

        # create result directory
        create_dir(Fpath, filename)

        # go to def generate_plots
        # generate plots and predict classes
        generate_plots(resultFinal, chr, Fpath, model, filename)

        if {"ballelefreq"}.issubset(finalreport.columns) or {"logrratio"}.issubset(
            finalreport.columns
        ):
            df = pd.read_csv(
                Fpath
                + "resultsPloidyScreen_"
                + os.path.splitext(os.path.basename(filename))[0]
                + "/results.txt",
                sep="\t",
            )
            df.columns = ["plot", "class"]
            df = df[df["class"] != "disomy"]
            replacement_dict = {
                Fpath + "/tmp/": "",
            }
            df["plot"] = df["plot"].replace(replacement_dict, regex=True)
            for x in ["ballelefreq", "logrratio"]:
                if {x}.issubset(finalreport.columns):
                    for ind in df.index:
                        tmp = df["plot"][ind].rsplit(".", 1)[0]
                        ind2= "_".join(tmp.split("_")[:-1])
                        finalreport["chromosome"] = finalreport["chromosome"].astype(
                            "str"
                        )
                        BAF = finalreport[
                            (finalreport["samplename"]==ind2) & (finalreport["chromosome"] == tmp.rsplit("_", 1)[1])
                        ]
                        sns.scatterplot(x="position", y=x, data=BAF)
                        if df["class"][ind] == "monosomy":
                            Bpath = (
                                Fpath
                                + "resultsPloidyScreen_"
                                + os.path.splitext(os.path.basename(filename))[0]
                                + "/monosomy/"
                            )
                        if df["class"][ind] == "trisomy":
                            Bpath = (
                                Fpath
                                + "resultsPloidyScreen_"
                                + os.path.splitext(os.path.basename(filename))[0]
                                + "/trisomy/"
                            )
                        plt.savefig(Bpath + tmp + "_" + x + ".pdf")
                        plt.close()
        return None

    except Exception as inst:
        print(f"Error in runPloidyScreen: {inst}", flush=True)
        return None
