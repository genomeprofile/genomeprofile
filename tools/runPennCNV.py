"""
Title: Genome profile.Pipeline to assess animals that have different genome profile 
Author: Wageningen Livestock Research

Module runPennCNV for GenomeProfile

Date: 16-08-2022
Last update: 29-08-2023


"""

import os.path
import shutil


def runPennCNV(filename, finalreport, path, Mfile, lastchr, BCfile):
    try:
        # PenCNV anaysis

        # delete output dir if exists
        file = os.path.splitext(os.path.basename(filename))[0]
        if os.path.isdir(path + "/PennCNV_output_" + file + "/") is True:
            print("remove directory PennCNV_output")
            shutil.rmtree(path + "/PennCNV_output_" + file + "/")

        # check for columnnames "snpname", "sampleID","ballelefreq","logrratio" 
        # in FinalReport for PennCNV
        colnames = ["snpname", "samplename", "ballelefreq", "logrratio"]
        for i in colnames:
            if {i}.isdisjoint(finalreport.columns):
                print("final report " + filename)
                print("The column " + i + " is missing in the final report", flush=True)
                print("No PennCNV analysis done")
                return None

        ### Step 1
        ### split Illumina report to individual files

        # Create temporary dir for individual files
        # delete temporary dir if exists
        if os.path.isdir(path + "/PennCNV_tmp/") is True:
            print("remove directory PennCNV_tmp")
            shutil.rmtree(path + "/PennCNV_tmp/")
        # Create temporary dir for individual files
        os.mkdir(path + "/PennCNV_tmp/")

        # Create output dir for individual files
        os.mkdir(path + "/PennCNV_output_" + file + "/")

        # create individual files
        samples = list(set((finalreport["samplename"])))

        for samp in samples:
            print(
                "Writing to temporary file PennCNV_tmp/" + str(samp) + ".txt",
             )
            filename = "PennCNV_tmp/" + str(samp) + ".txt"
            subset = finalreport[finalreport["samplename"] == samp]
            subset = subset[["snpname", "logrratio", "ballelefreq"]]
            #subset = subset[subset["logrratio"] >= 0]
            #subset = subset[subset["ballelefreq"] >= 0]
            subset.columns = [
                "Name",
                str(samp) + ".Log R Ratio",
                str(samp) + ".B Allele Freq",
            ]
            with open(filename, "w", encoding="utf-8"):
                subset.to_csv(filename, header=True, index=None, sep="\t", mode="w")
        print("Done; Finished splitting Illumina report")

        ### Step 2
        ### compile PFB file from multiple signal intensity files containing BAF values
        ### using compile_pfb.pl from PennCNV

        # create list of files
        listfiles = list(
            map(
                lambda orig_string: "./PennCNV_tmp/" + str(orig_string) + ".txt",
                samples,
            )
        )
        with open("list.txt", "w", encoding="utf-8") as fp:
            fp.write("\n".join(listfiles))

        # The output is a .pfb file
        print("Start compiling PFB")
        os.system(
            r"./tools/compile_pfb.exe -listfile list.txt -snpposfile "
            + Mfile
            + "  -output pfb_file.pfb"
        )

        # Step 3
        # generate CNV calls from SNP genotyping data that contains
        # Log R Ratio and B Allele Frequency for each SNP or CN marker.
        # using detect_cnv.pl from PennCNV

        # Call CNVs containing more than or equal to 3 SNPs
        minsnp = 3
        # Last chromosome nr
        hmm = "./PennCNV-1.0.5/lib/hhall.hmm"
        log_file1 = "./PennCNV_output_" + file + "/hmm.minsnp_" + str(minsnp) + ".log"
        raw_file1 = (
            "./PennCNV_output_" + file + "/hmm.minsnp_" + str(minsnp) + ".rawcnv"
        )
        print("Start detecting CNVs")
        os.system(
            r"./tools/detect_cnv.exe -verbose -hmm "
            + hmm
            + " -pfb pfb_file.pfb -lastchr "
            + str(lastchr)
            + " --listfile list.txt -log "
            + log_file1
            + " -out "
            + raw_file1
            + " -test"
        )
        print("Done; Finished detecting CNVs")

        # Step 4
        # filter CNV calls from input by various criteria
        # using filter_cnv.pl from PennCNV
        qc_sdLRR = 0.3
        qc_maxLen = "5m"
        qc_passout = (
            "./PennCNV_output_" + file + "/hmm_minsnp_" + str(minsnp) + ".qcpass"
        )
        qc_sumout = "./PennCNV_output_" + file + "/hmm_minsnp_" + str(minsnp) + ".qcsum"
        qc_goodcnv = (
            "./PennCNV_output_" + file + "/hmm_minsnp_" + str(minsnp) + ".goodcnv"
        )

        print("Start filtering CNVs")
        os.system(
            r"./tools/filter_cnv.exe "
            + raw_file1
            + " -qclogfile "
            + log_file1
            + " -qclrrsd "
            + str(qc_sdLRR)
            + " -maxlength "
            + qc_maxLen
            + " -qcpassout "
            + qc_passout
            + " -qcsumout "
            + qc_sumout
            + " -out "
            + qc_goodcnv
        )
        print("Done; Finished filtering CNVs")

        # Step 5
        # Create output report
        os.system(
            "Rscript tools/pennCNV.Rscript " + qc_goodcnv + " " + BCfile + " " + file
        )

        shutil.rmtree(path + "/PennCNV_tmp/")

        os.remove("pfb_file.pfb")

        return None

    except Exception as inst:
        print(f"Error in runPennCNV: {inst}")
        return None
