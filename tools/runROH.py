"""
Title: Genome profile.Pipeline to assess animals that have different genome profile 
Author: Wageningen Livestock Research

Module runROH for GenomeProfile

Date: 16-08-2022
Last update: 13-06-2024


"""

import os
import shutil

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def readROHInp():
    """
            Read ROH input (inp) file
    Inputs:
    * input file: "ROH.inp"
            * settings for plink
    Returns:
    * settings for plink
    """
    try:
        if os.path.isfile("ROH.inp"):
            # read inp fiel
            with open("ROH.inp", "r", encoding="utf-8") as f:
                inp = f.readlines()
                # setting for plink
                Hd = inp[0].rstrip()
                Hwh = inp[1].rstrip()
                Hkb = inp[2].rstrip()
                Hws = inp[3].rstrip()
                return (Hd, Hwh, Hkb, Hws)
        else:
            print("The file 'ROH.inp' does not exists print")
            return None
    except Exception as inst:
        print(f"Error in readInp: {inst}")
        return None


def PlinkROH(settings, path, file, nChr):
    """
            Run plink
    Inputs:
    * param settings: settings for plink
    * param file: file for plink
    Outputs:
    *results ROH plink
    """
    try:
        out = (
            path
            + "plink_output_"
            + os.path.splitext(os.path.basename(file))[0]
            + "/"
            + file
            + "_plink"
        )
        os.system(
            r"./tools/plink \
			--homozyg group \
			--file {} \
			--homozyg-density {} \
			--homozyg-window-het {} \
			--homozyg-kb {} \
			--homozyg-window-snp {} \
			--allow-no-sex \
			--out {} \
			--recode \
			--chr-set {}".format(
                file, settings[0], settings[1], settings[2], settings[3], out, nChr
            )
        )
        ## Generate ROH report
        os.system("Rscript tools/detectRUNS.Rscript " + out + " " + str(nChr))
        return None
    except Exception as inst:
        print(f"Could not run plink: {inst}")
        return None


def PlinkHet(path, file, nChr):
    """
    Run plink
    Inputs:
    * param file: file voor plink
    Outputs
    *results heterozygosity plink
    """

    try:
        out = (
            path
            + "plink_output_"
            + os.path.splitext(os.path.basename(file))[0]
            + "/"
            + file
            + "_plink"
        )

        os.system(
            r"./tools/plink \
			--file {} \
			--het \
			--out {} \
			--chr-set {}".format(
                file, out, nChr
            )
        )
        ## generate plot
        os.system(
            """awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' """
            + out
            + ".het > tmp.het"
            ""
        )
        df = pd.read_csv("tmp.het", sep="\t")
        sns.boxplot(x="FID", y="F", data=df)
        plt.savefig(out + "_plink.het.pdf")
        os.system("rm tmp.het")
        plt.close()
        return None

    except Exception as inst:
        print(f"Could not run plink: {inst}")
        return None


def PlinkIBS(path, file,nChr):
    """
    Run plink
    Inputs:
    * param file: file voor plink
    Outputs
    *results distance square 1-ibs plink
    """

    try:
        out = (
            path
            + "plink_output_"
            + os.path.splitext(os.path.basename(file))[0]
            + "/"
            + file
            + "_plink"
        )

        os.system(
            r"./tools/plink \
			--file {} \
			--distance square 1-ibs \
			--out {} \
            -chr-set {}".format(
                file, out,nChr
            )
        )
        ## Generate IBS report
        os.system("Rscript tools/IBS.Rscript " + out)
        return None


    except Exception as inst:
        print(f"Could not run plink: {inst}")
        return None




def runPlink(path, file, nChr):
    """
    Inputs:
    * param settings: settings for plink
    * param file: file for plink
    Output:
    * results heterozygosity plink
    * results ROH plink
    """
    try:
        # Create output dir for iplink
        # delete output dir plink if exists
        print(
            path + "/plink_output_" + os.path.splitext(os.path.basename(file))[0] + "/"
        )
        if (
            os.path.isdir(
                path
                + "/plink_output_"
                + os.path.splitext(os.path.basename(file))[0]
                + "/"
            )
            is True
        ):
            print("remove directory plink_output")
            shutil.rmtree(
                path
                + "/plink_output_"
                + os.path.splitext(os.path.basename(file))[0]
                + "/"
            )
        # create output directory plink
        os.mkdir(
            path + "plink_output_" + os.path.splitext(os.path.basename(file))[0] + "/"
        )

        print("read settings plink")
        settings = readROHInp()
        # run plink

        print("Start plink analysis - inbreeding coefficient")
        PlinkHet(path, file, nChr)
        print("Done; Finished plink analysis- inbreeding coefficient")
        print("Start plink analysis - ROH")
        PlinkROH(settings, path, file, nChr)
        print("Done; Finished plink analysis - ROH")
        print("Start plink analysis - IBS")
        PlinkIBS(path, file,nChr)
        print("Done; Finished plink analysis - IBS")


        # delete ped and map file
        os.remove(file + ".ped")
        os.remove(file + ".map")
        return None

    except Exception as inst:
        print(f"Error in readFiles: {inst}")
        return None
